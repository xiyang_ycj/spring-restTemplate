package com.xy.csdn.cfg;

import org.omg.PortableInterceptor.ClientRequestInterceptor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;


import java.io.IOException;

/**
 * 自定义请求拦截器
 * @author xiyang.ycj
 * @since May 30, 2019 10:59:45 AM
 */
public class CustomInterceptor implements ClientHttpRequestInterceptor {
    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpHeaders headers = request.getHeaders();

        // 加入自定义字段
        headers.add("xy","1024" );

        // 请求继续被执行
        return execution.execute(request, body);

    }
}
