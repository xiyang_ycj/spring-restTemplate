package com.xy.csdn.cfg;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xiyang.ycj
 * @since May 30, 2019 15:11:35 PM
 */
@Configuration
@ConditionalOnClass(HttpClient.class)//该注解的参数对应的类必须存在，否则不解析该注解修饰的配置类
public class HttpClientAutoConfig {

    @Bean
    @ConditionalOnMissingBean(HttpClient.class)//该注解表示，如果存在它修饰的类的bean,则不需要创建这个bean
    public HttpClient httpClient(){
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(1000)
                .setSocketTimeout(1000000)
                .build();
        return HttpClientBuilder.create()
                .setDefaultRequestConfig(requestConfig)
                .setUserAgent("agent")
                .setMaxConnPerRoute(10)
                .setMaxConnTotal(50)
                .build();
    }
}
