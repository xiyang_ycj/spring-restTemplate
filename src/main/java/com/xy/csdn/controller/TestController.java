package com.xy.csdn.controller;

import com.xy.csdn.common.BaseResult;
import com.xy.csdn.common.ReturnCode;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

/**
 * @author xiyang.ycj
 * @since May 23, 2019 17:13:52 PM
 */
@RestController
public class TestController {

    @RequestMapping("/oneGet")
    public Object oneGet(HttpServletRequest  request,@RequestHeader("xy")String xy){
        String authorization = request.getHeader("authorization");
        System.out.println(authorization);
        System.out.println(xy);
        return new BaseResult(ReturnCode.SUCCESS.getCode(),"","oneGet");
    }

    @RequestMapping("/oneGet/{id}")
    public Object oneGet1(@PathVariable("id")String id){
        return new BaseResult(ReturnCode.SUCCESS.getCode(),"","oneGet1--"+id);
    }

    @RequestMapping("/twoGet")
    public Object twoGet(@RequestParam("id")String id){
        return new BaseResult(ReturnCode.SUCCESS.getCode(),"","twoGet--"+id);
    }

    @RequestMapping("/threeGet")
    public Object threeGet(){
        return new BaseResult(ReturnCode.SUCCESS.getCode(),"","threeGet");
    }

    @PostMapping(value = "/onePostLocation")
    public void onePostLocation(HttpServletResponse response,@RequestParam(value = "id",required = false)String id){
        if(id!=null){
            response.setHeader("Location","https://blog.csdn.net/YCJ_xiyang/column/info/38209"+id);
        }else {
            response.setHeader("Location","https://blog.csdn.net/YCJ_xiyang/column/info/38209");
        }
        response.setStatus(HttpStatus.FOUND.value());
    }

    @PostMapping(value = "/onePost")
    public Object onePost(@RequestParam(value = "id",required = false)String id,@RequestBody String str){
        return new BaseResult(ReturnCode.SUCCESS.getCode(),"","onePost"+id+"----"+str);
    }

    @PostMapping(value = "/onePostEntity",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)//注意，只有当content-type:application/json时，才可以用@requestBody绑定数据
    public Object onePostEntity(@RequestParam(value = "id",required = false)String id,@RequestParam("name")String name){
        return new BaseResult(ReturnCode.SUCCESS.getCode(),"","onePostEntity"+id+"----"+name);
    }

    @PostMapping(value = "/onePostEntitys",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Object onePostEntitys(@RequestParam(value = "id",required = false)String id,@RequestParam("name")String name){
        return new BaseResult(ReturnCode.SUCCESS.getCode(),"","onePostEntity"+id+"----"+name);
    }

    @PutMapping(value = "/put")
    public void put(@RequestParam(value = "id",required = false)String id){
        System.out.println("put"+id);
    }

    @PatchMapping("/patch")
    public Object patch(@RequestParam("id")String id){
        return new BaseResult(ReturnCode.SUCCESS.getCode(),"","patch--"+id);
    }

    @DeleteMapping(value = "/delete")
    public void delete(@RequestParam(value = "id",required = false)String id){
        System.out.println("delete"+id);
    }

    @RequestMapping("/optionsForAllow")
    public Object optionsForAllow(@RequestParam(value = "id",required = false)String id){
        return new BaseResult(ReturnCode.SUCCESS.getCode(),"","optionsForAllow"+id);
    }

    @GetMapping(value = "/exchange",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public Object exchange(@RequestParam(value = "id",required = false)String id){
        return new BaseResult(ReturnCode.SUCCESS.getCode(),"","exchange"+id);
    }
}
