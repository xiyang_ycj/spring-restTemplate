package com.xy.csdn;

import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CsdnApplicationTests {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void contextLoads() throws URISyntaxException {
        System.out.println("====================getForObject=======================");
        //public <T> T getForObject(String url, Class<T> responseType, Object... uriVariables)//Object... 为可变参数
        String oneGet = restTemplate.getForObject("http://localhost:6788/oneGet", String.class);
        System.out.println(oneGet);
        //可以用一个数字做占位符，最后是一个可变长度的参数，来一一替换前面的占位符
        String oneGet1 = restTemplate.getForObject("http://localhost:6788/oneGet/{1}", String.class,"1");
        System.out.println(oneGet1);
        HashMap<String, Object> twoGetMap = new HashMap<>();
        twoGetMap.put("id","1");
        //public <T> T getForObject(String url, Class<T> responseType, Map<String, ?> uriVariables)
        //也可以前面使用id={id}这种形式，最后一个参数是一个map，map的key即为前边占位符的名字，map的value为参数值
        String twoGet = restTemplate.getForObject("http://localhost:6788/twoGet?id={id}", String.class,twoGetMap);
        System.out.println(twoGet);
        //public <T> T getForObject(URI url, Class<T> responseType)
        String threeGet = restTemplate.getForObject(new URI("http://localhost:6788/threeGet"), String.class);
        System.out.println(threeGet);
        System.out.println("====================getForEntity=======================");
        ResponseEntity<String> oneGetEntity = restTemplate.getForEntity("http://localhost:6788/oneGet", String.class);
        System.out.println(oneGetEntity);
        ResponseEntity<String> oneGetEntity1 = restTemplate.getForEntity("http://localhost:6788/oneGet/{1}", String.class, "1");
        System.out.println(oneGetEntity1);
        HashMap<String, Object> twoGetMapEntity = new HashMap<>();
        twoGetMapEntity.put("id","1");
        //也可以前面使用id={id}这种形式，最后一个参数是一个map，map的key即为前边占位符的名字，map的value为参数值
        ResponseEntity<String> twoGetEntity = restTemplate.getForEntity("http://localhost:6788/twoGet?id={id}", String.class,twoGetMapEntity);
        System.out.println(twoGetEntity);
        ResponseEntity<String> threeGetEntity = restTemplate.getForEntity(new URI("http://localhost:6788/threeGet"), String.class);
        System.out.println(threeGetEntity);
        System.out.println("====================headForHeaders=======================");
        HttpHeaders oneHeaders = restTemplate.headForHeaders("http://localhost:6788/oneGet");
        System.out.println(oneHeaders);
        HashMap<String, Object> headMap = new HashMap<>();
        headMap.put("id","1");
        HttpHeaders twoHeaders = restTemplate.headForHeaders("http://localhost:6788/twoGet?id={id}", headMap);
        System.out.println(twoHeaders);
        HttpHeaders threeHeaders = restTemplate.headForHeaders(new URI("http://localhost:6788/oneGet"));
        System.out.println(threeHeaders);
        System.out.println("====================postForLocation=======================");
        HttpEntity<Object> entity = new HttpEntity<>(null,null);
        URI oneUri = restTemplate.postForLocation("http://localhost:6788/onePostLocation",entity);
        System.out.println(oneUri);
        URI oneUri1 = restTemplate.postForLocation("http://localhost:6788/onePostLocation?id={id}",entity,"---1");
        System.out.println(oneUri1);
        HashMap<String, Object> uriMap = new HashMap<>();
        uriMap.put("id","---1");
        URI twoUri = restTemplate.postForLocation("http://localhost:6788/onePostLocation?id={id}", entity, uriMap);
        System.out.println(twoUri);
        URI threeUri = restTemplate.postForLocation(new URI("http://localhost:6788/onePostLocation"), entity);
        System.out.println(threeUri);
        System.out.println("====================postForObject=======================");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        Gson gson = new Gson();
        Map<String,String> postMaps = new HashMap<>();
        postMaps.put("name","cat");
        HttpEntity<Object> postEntity = new HttpEntity<>(gson.toJson(postMaps),headers);
        String onePost = restTemplate.postForObject("http://localhost:6788/onePost",postEntity,String.class);
        System.out.println(onePost);
        String onePost1 = restTemplate.postForObject("http://localhost:6788/onePost?id={id}",postEntity,String.class,"---1");
        System.out.println(onePost1);
        HashMap<String, Object> postMap = new HashMap<>();
        postMap.put("id","---1");
        String twoPost = restTemplate.postForObject("http://localhost:6788/onePost?id={id}", postEntity,String.class, postMap);
        System.out.println(twoPost);
        String threePost = restTemplate.postForObject(new URI("http://localhost:6788/onePost"), postEntity,String.class);
        System.out.println(threePost);
        System.out.println("====================postForEntity=======================");
        HttpHeaders postHeadersEntity = new HttpHeaders();
        postHeadersEntity.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        MultiValueMap<String,String> body = new LinkedMultiValueMap<>();
        body.add("name","tom");
        HttpEntity<Object> postEntitys = new HttpEntity<>(body,postHeadersEntity);
        ResponseEntity<String> onePostEntity = restTemplate.postForEntity("http://localhost:6788/onePostEntity", postEntitys, String.class);
        System.out.println(onePostEntity);
        ResponseEntity<String> onePostEntity1 = restTemplate.postForEntity("http://localhost:6788/onePostEntity?id={id}",postEntitys,String.class,"---1");
        System.out.println(onePostEntity1);
        HashMap<String, Object> postMapEntity = new HashMap<>();
        postMapEntity.put("id","---1");
        ResponseEntity<String> twoPostEntity = restTemplate.postForEntity("http://localhost:6788/onePostEntity?id={id}", postEntitys,String.class, postMapEntity);
        System.out.println(twoPostEntity);
        ResponseEntity<String> threePostEntity = restTemplate.postForEntity(new URI("http://localhost:6788/onePostEntity"), postEntitys,String.class);
        System.out.println(threePostEntity);
        System.out.println("====================postForEntity扩展=======================");
        HttpHeaders postHeadersEntitys = new HttpHeaders();
        postHeadersEntitys.add("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
        MultiValueMap<String,String> bodys = new LinkedMultiValueMap<>();
        bodys.add("name","sam");
        HttpEntity<Object> postEntityss = new HttpEntity<>(bodys,postHeadersEntitys);
        ResponseEntity<String> onePostEntitys = restTemplate.postForEntity("http://localhost:6788/onePostEntitys", postEntityss, String.class);
        System.out.println(onePostEntitys);
        System.out.println("====================put=======================");
        HttpHeaders putHeaders = new HttpHeaders();
        putHeaders.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<Object> putEntity = new HttpEntity<>(null,putHeaders);
        restTemplate.put("http://localhost:6788/put?id={id}",putEntity,"--1");
        System.out.println("====================patch=======================");
        HttpHeaders patchHeaders = new HttpHeaders();
        patchHeaders.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<Object> patchEntity = new HttpEntity<>(null,patchHeaders);
        String patch = restTemplate.patchForObject("http://localhost:6788/patch?id={id}", patchEntity, String.class, "--1");
        System.out.println(patch);
        System.out.println("====================delete=======================");
        restTemplate.delete("http://localhost:6788/delete?id={id}",  "--1");
        System.out.println("====================optionsForAllow=======================");
        Set<HttpMethod> httpMethods = restTemplate.optionsForAllow("http://localhost:6788/optionsForAllow?id={id}", "--1");
        System.out.println(httpMethods);
        System.out.println("====================exchange=======================");
        HttpHeaders exchangeHeaders = new HttpHeaders();
        exchangeHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        URI url = new URI("http://localhost:6788/exchange?id=--1");
        RequestEntity<Object> orderEntity = new RequestEntity<>(exchangeHeaders,HttpMethod.GET,url);
        ResponseEntity<String> result = restTemplate.exchange(orderEntity, String.class);
        System.out.println(result);
    }

}
